const CDP = require('chrome-remote-interface');
const robot = require('robotjs'); // Control mouse / Play, Pause and whatnot:
const ffi = require('ffi')

robot.setMouseDelay(2);

// robot.typeString("Hello World");

// robot.keyTap("enter");

// robot.moveMouse(x, y)

function computerSleep() {
    //exec('rundll32.exe powrprof.dll,SetSuspendState 0,1,0', {}, function(error, stdout, stderr){});
    ffi.Library('powrprof.dll', {
        SetSuspendState: ['int', ['int', 'int', 'int']]
    }).SetSuspendState(0, 0, 0);
}


/*
CDP((client) => {
    // extract domains
    const {Network, Page} = client;
    // setup handlers
    Page.loadEventFired(() => {
        client.close();
    });
    // enable events then start!
    Promise.all([
        Network.enable(),
        Page.enable()
    ]).then(() => {
        return Page.navigate({url: process.argv[2]});
    }).catch((err) => {
        console.error(err);
        client.close();
    });
}).on('error', (err) => {
    // cannot connect to the remote endpoint
    console.error(err);
});
//*/